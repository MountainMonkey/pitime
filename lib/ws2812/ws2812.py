import neopixel
import math
from machine import Pin
from utime import sleep

class Matrix:
    def __init__(self, xres, yres, pin):
        self.yres = yres
        self.xres = xres
        self.pin = pin

        self.wall = neopixel.NeoPixel(Pin(pin), xres * yres)
        self.wall.write()

    def mapPixel(self, x,y):
        return y * self.yres + x
        #if y % 2 == 1:
        #    return self.xres * y + x
        #else:
        #    return self.xres * y + self.xres - 1 - x
    
    def test(self):
        for x in range(9):
            r = 255
            g = 0
            b = 0
            self.wall[x] = (r >> 3, g >> 3, b >> 4)
            self.wall[x+8] = (r >> 3, g >> 3, b >> 4)
            self.wall.write()
            sleep(1)  
        
    def start(self,picture):
        a0 = 0
        a1 = 0

        for idy, y in enumerate(picture):
            for idx, x in enumerate(y):
                if x == 1:
                    print(self.mapPixel(idx, idy))
                    r = 255
                    g = 0
                    b = 0
                    self.wall[self.mapPixel(idx, idy)] = (r >> 3, g >> 3, b >> 4)
                    self.wall.write()
                elif x == 2:
                    print(self.mapPixel(idx, idy))
                    r = 0
                    g = 255
                    b = 0
                    self.wall[self.mapPixel(idx, idy)] = (r >> 3, g >> 3, b >> 4)
                    self.wall.write()
                else:
                    print(self.mapPixel(idx, idy))
                    r = 0
                    g = 0
                    b = 255
                    self.wall[self.mapPixel(idx, idy)] = (r >> 3, g >> 3, b >> 4)
                    self.wall.write()


    def breath(self):
        a0 = 0
        a1 = 0
        while True:
            a0 += 0.1
            a1 += 0.2
            for y in range(self.yres):
                for x in range(self.xres):
                    r = 128 + math.floor((math.sin(a0 + x * 0.4) + math.cos(a1 + y * 0.4)) * 63)
                    g = 128 + math.floor((math.sin(a0 + y * 0.4) + math.cos(a1 + x * 0.4)) * 63)
                    b = 255 - r
                    self.wall[self.mapPixel(x,y)] = (r >> 3, g >> 3, b >> 4)
            self.wall.write()