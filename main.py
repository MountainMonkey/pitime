from machine import Pin, I2C
from utime import sleep

from lib.imu import MPU6050
#import time
#from lib.apds9960LITE import APDS9960LITE
from lib.apds9960.apds9960 import uAPDS9960 as APDS9960
from lib.apds9960.const import *

from lib.ws2812.ws2812 import Matrix

#Gyro
pin = Pin("LED", Pin.OUT)
i2cGyro = I2C(0, sda=Pin(0), scl=Pin(1), freq=400000)
imu = MPU6050(i2cGyro)

#Geste
i2cGest =  I2C(1, sda=Pin(2), scl=Pin(3) )
apds9960=APDS9960(i2cGest)      

#LED Display
matrix = Matrix(8,8,6)

dirs = {
    APDS9960_DIR_NONE: "none",
    APDS9960_DIR_LEFT: "left",
    APDS9960_DIR_RIGHT: "right",
    APDS9960_DIR_UP: "up",
    APDS9960_DIR_DOWN: "down",
    APDS9960_DIR_NEAR: "near",
    APDS9960_DIR_FAR: "far",
}


def directionSpin(x,y,z):
    if x > 0.8 and y >= -0.1 and y <= 0.1 and z >= -0.1 and z <= 0.1:
        print("facing back",end="\n")
    elif x < -0.8 and y >= -0.1 and y <= 0.1 and z >= -0.1 and z <= 0.1:
        print("facing front",end="\n")
    elif x <= 0.1 and x >= -0.1 and y < -0.8 and z >= -0.1 and z <= 0.1:
        print("facing right",end="\n")
    elif x <= 0.1 and x >= -0.1 and y > 0.8 and z >= -0.1 and z <= 0.1:
        print("facing left",end="\n")
    elif x <= 0.1 and x >= -0.1 and z >= -0.1 and y <= 0.1 and z > 0.8:
        print("facing up",end="\n")
    elif x <= 0.1 and x >= -0.1 and z >= -0.1 and y <= 0.1 and z < -0.8:
        print("facing down",end="\n")
    else:
        print("moving ...")



def main():
    print("Hello")
    dot = [
        [0,0,1,1,1,1,0,0],
        [0,1,0,0,0,0,1,0],
        [1,0,1,0,1,0,0,1],
        [1,0,1,0,1,0,0,1],
        [1,0,0,0,0,1,0,1],
        [1,0,1,1,1,0,0,1],
        [0,1,0,0,0,0,1,0],
        [0,0,1,1,1,1,0,0],
        ]
    matrix.start(dot)

    #print("gesture")
    #print(i2cGest)
    #print(i2cGest.scan(),' (decimal)')
    #print(hex(i2cGest.scan()[0]), ' (hex)')
    #apds9960.enableGestureSensor()
    #while True:
    #    if apds9960.isGestureAvailable():
    #        motion = apds9960.readGesture()
    #        print("Gesture={}".format(dirs.get(motion, "unknown")))
    
    
    #print("start to recognize directions tipping")
    #while True:
    #    pin.toggle()
    #    ax=round(imu.accel.x,1)
    #    ay=round(imu.accel.y,1)
    #    az=round(imu.accel.z,1)
    #    gx=round(imu.gyro.x)
    #    gy=round(imu.gyro.y)
    #    gz=round(imu.gyro.z)
    #    tem=round(imu.temperature,2)
        # print(ax,"\t",ay,"\t",az)
        # print(gx,"\t",gy,"\t",gz,"\t""\t""\t",end="\r")
    #    directionSpin(ax,ay,az)
    #    sleep(1) # sleep 1sec
    


if __name__ == "__main__":
    main()
